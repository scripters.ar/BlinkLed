# Hola Mundo Arduino

## Introducción

En este proyecto esta el material necesario para realizar nuestro primer proyecto con Arduino, que consistira en hacer parpadear un Led.

## Materiales

* **Una placa Arduino**
* **Una resistencia de 220Ω**
* **Un diodo LED**
* **Un Protoboard**

## Uso

```
En la carpeta diagrams encontraras la conexion del circuito.
```

```
En la carpeta src encontraras el codigo fuente de ejemplo.
```

## Autor

* **Edermar Dominguez** - [Ederdoski](https://github.com/ederdoski)

## License

This code is open-sourced software licensed under the [MIT license.](https://opensource.org/licenses/MIT)