/*
  Blink Led
  Este programa enciende y apaga por un segundo un LED 
  conectado en el pin 2 de nuestro Arduino.
 */

int LED = 2;

void setup() { 
  pinMode(LED, OUTPUT); 
}

void loop() { 

  digitalWrite(LED, HIGH);   
  delay(1000);              
  digitalWrite(LED, LOW);    
  delay(1000);
  
}
